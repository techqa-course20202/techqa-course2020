package com.company;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Shape> shapeList = new ArrayList<>();

        boolean switcher = true;

        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("What is the form? (ex. circle or rectangle)");
            String incomeName = sc.next();

            if (incomeName.equalsIgnoreCase(enumStaff.circle.toString())) {
                System.out.println("OK, its a circle. Enter the radius of circle: ");
                double r = sc.nextDouble();
                circle circle1 = new circle(r);
                shapeList.add(circle1);
                for (Shape shape : shapeList){
                    System.out.println("The area of " + shape.nameOfForm() + "is " + shape.calculateArea());
                    System.out.println("The perimeter of " + shape.nameOfForm() + "is " + shape.calculatePerimeter());
                }
            } else {
                if (incomeName.equalsIgnoreCase(enumStaff.rectangle.toString())) {
                    System.out.println("OK, its a rectangle.");
                    System.out.print("Enter HIGHT: ");
                    double hight = sc.nextDouble();
                    System.out.print("Enter WIGHT: ");
                    double wight = sc.nextDouble();
                    rectangle rectangle1 = new rectangle(wight, hight);
                    shapeList.add(rectangle1);
                    for (Shape shape : shapeList){
                        System.out.println("The area of " + shape.nameOfForm() + "is " + shape.calculateArea());
                        System.out.println("The perimeter of " + shape.nameOfForm() + "is " + shape.calculatePerimeter());
                    }
                } else {
                    System.out.println("Try again.");
                    switcher = false;
                }
            }
        } while (!switcher);
    }
}
