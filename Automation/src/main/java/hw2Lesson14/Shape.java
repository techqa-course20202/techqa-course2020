package com.company;

public abstract class Shape {

    public abstract String nameOfForm();

    public abstract double calculateArea();
    public abstract double calculatePerimeter();

}