package com.company;

import java.lang.Math;

public class circle extends Shape implements perimetr{

    private double radius;
    private double calculateArea;
    private double calculatePerimeter;

    public circle(double radius){
        this.radius=radius;
    }

    public double getRadius(){
        this.radius=radius;
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String nameOfForm() {
        String nameOfForm = "CIRCLE ";
        return nameOfForm;
    }

    @Override
    public double calculateArea() {
        return (Math.PI*(radius*radius));
    }

    @Override
    public double calculatePerimeter() {
        return (2*Math.PI*radius);
    }
}

