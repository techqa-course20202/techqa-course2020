package com.company;

public class rectangle extends Shape implements perimetr {

    private double wight;
    private double height;
    private double calculateArea;
    private double calculatePerimeter;

    public rectangle(double wight, double height) {
        this.wight = wight;
        this.height = height;
    }

    public double getWight(){
        this.wight=wight;
        return wight;
    }

    public double getHeight(){
        this.height=height;
        return height;
    }

    public void setWidth(double width) {
        if (width > 0) {
            this.wight = width;
        }
    }
    public void setHeight(double height) {
        if (height > 0) {
            this.height = height;
        }
    }


    @Override
    public String nameOfForm() {
        String nameOfRectangle = "RECTANGLE ";
        return nameOfRectangle;
    }

    @Override
    public double calculateArea() {
        return wight*height;
    }

    @Override
    public double calculatePerimeter() {
        return ((wight+height)*2);
    }
}
