package hw1Lesson13;

import java.util.Scanner;

    class L13 {


        public static void main(String[] args) {

            boolean switcher;

            do {
                Scanner in = new Scanner(System.in);
                System.out.print("Input phone: ");
                String PhoneN = in.nextLine();

                String regex = "\\d{9,13}";
                System.out.println();
                if (PhoneN.matches(regex)){
                    System.out.println("Success ");
                    switcher = true;
                } else {
                    System.out.println("Упсі! Схоже, ти ввів не валідний номер мобільного, спробуй ще раз:)");
                    switcher = false;
                }
            } while (!switcher);
        }
    }
